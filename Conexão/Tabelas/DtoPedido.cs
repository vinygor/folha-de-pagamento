﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    public class DtoPedido
    {
        public int Id { get; set; }
        public string cpf { get; set; }
        public string tipodepedido { get; set; }
        public string rg { get; set; }
        public string funcionario { get; set; }
    }
}
