﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    public class DtoCategoria
    {
        public int Id { get; set; }
        public string adolecente { get; set; }
        public string adulto { get; set; }
        public string crianca { get; set; }
    }
}
