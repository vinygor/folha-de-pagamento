﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Conexão.Classes_Basicas
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=MusicaDB;uid=root;password=1234";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            return connection;
        }

    }
}
