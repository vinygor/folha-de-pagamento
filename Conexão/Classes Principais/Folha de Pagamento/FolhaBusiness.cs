﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Conexão.Classes_Principais.Folha_de_Pagamento
{
    public class FolhaBusiness
    {
        public void salvar(FolhaDTO i)
        {
            FolhaDatabase bd = new FolhaDatabase();
            bd.Salva(i);
        }
        public List<FolhaDTO> Consultar(string nome, string cpf)
        {
            FolhaDatabase db = new FolhaDatabase();
            return db.Consultar(nome, cpf);
        }

        public void Remover(int id)
        {
            FolhaDatabase db = new FolhaDatabase();
            db.Remover(id);
        }


    }
}
