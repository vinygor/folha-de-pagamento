﻿using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    public class BusinessFuncionario
    {
        public DtoFuncionario Log(string login, string senha)
        {
            if (login == string.Empty)
            {
                throw new ArgumentException("user obrigatorio");
            }
            if (senha == string.Empty)
            {
                throw new ArgumentException("a senha é obrigatoria");
            }

            DatabaseFuncionario db = new DatabaseFuncionario();
            return db.Log(login, senha);
        }

        DatabaseFuncionario db = new DatabaseFuncionario();
        public void Remover(int id)
        {
            db.Remover(id);
            
        }
        public void alterar(DtoFuncionario sen)
        {
            DatabaseFuncionario db = new DatabaseFuncionario();
            db.alterar(sen);
        }

    }
}
