﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    public class DtoFuncionario
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string login { get; set; }
        public string senha { get; set; }
        public bool permiadm { get; set; }
        public bool permiproduto { get; set; }
        public bool permipedido { get; set; }

    }
}
