﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace progamin_chatu.Telinhas.Tabelas
{
    class DatabaseSistema
    {
  
        public bool Logar(string usuario, string senha)
        {
            string script =
                
                @"select * 
                    from tb_log
                    where nm_usuario = @nm_usuario
                    and ds_senha  = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", usuario));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader read = db.ExecuteSelectScript(script, parms);

            if (read.Read())
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
