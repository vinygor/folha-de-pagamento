﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    class DatabaseRoupas
    {
        public int salvar(DtoRoupas savler)
        {
            string script = @"Insert Into tb_GRstore (nm_roupa, vl_numerecao) Values (@nm_roupas, @vl_numeracao)";
            List<MySqlParameter> roma = new List<MySqlParameter>();
            roma.Add(new MySqlParameter("nm_roupa", savler.roupa));
            roma.Add(new MySqlParameter("vl_numeracao", savler.numeracao));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, roma);
        }
    }
}
