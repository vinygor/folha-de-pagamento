﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    public class DtoCliente
    {
        public int cliente { get; set; }
        public int cpf { get; set; }
        public int cartao { get; set; }
        public string enderco { get; set; }
        public string nome { get; set; }
        public string rg { get; set; }
    }
}
