﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Conexão.Classes_Principais.Fornecedor
{
    class DatabaseFornecedor
    {
        public int forner(DTOfornecedor distribuicao)
        {
            string script = 
            @"Insert into Tb_GRstore (nm_nome, vl_numerotel, ds_cidade)
            Values (@nm_nome, @vl_numerotel, @ds_cidade)";

            List<MySqlParameter> pescara = new List<MySqlParameter>();
            pescara.Add(new MySqlParameter("nm_nome", distribuicao.nome));
            pescara.Add(new MySqlParameter("vl_numerotel", distribuicao.numerotel));
            pescara.Add(new MySqlParameter("ds_cidade", distribuicao.cidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, pescara);
            

        }
    }
}
