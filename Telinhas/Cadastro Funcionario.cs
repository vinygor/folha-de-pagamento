﻿using progamin_chatu.Conexão.Classes_Principais.Outras;
using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class cadastro_Funcionario : Form
    {
        public cadastro_Funcionario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BusinessFuncionario business = new BusinessFuncionario();
            DtoFuncionario funcionario = business.Log(txtnuser.Text, txtsenha.Text);

            if (funcionario != null)
            {
                UserSession.UserLogado = funcionario;

                Menu menu = new Menu();
                menu.Show();
                this.Hide();

            }
            else
            {
                MessageBox.Show("Credenciais inválidas.", "GIRLS STORE",
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
    }
}
