﻿using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class CadastrarProduto : Form
    {
        public CadastrarProduto()
        {
            InitializeComponent();
        }

        private void CadastrarProduto_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DtoRoupas ross = new DtoRoupas();
                ross.roupa = txtnome.Text;
                ross.numeracao = Convert.ToInt32(txtnumeracao.Text);
                ross.categoria = txtcategoria.Text;

                BusinessRoupas buss = new BusinessRoupas();
                buss.salvar(ross);
                enviarmensagem("produto salvo no banco!");
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void enviarmensagem(string message)
        {
            MessageBox.Show(message, "produto salvo", MessageBoxButtons.OK);
        }
    }
}
